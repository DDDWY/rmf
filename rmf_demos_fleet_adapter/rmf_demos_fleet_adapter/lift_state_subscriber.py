import sys
import threading

import rclpy
from rclpy.node import Node
from rmf_lift_msgs.msg import LiftState
from fastapi import FastAPI
import uvicorn
import requests
from typing import Optional
from pydantic import BaseModel
app = FastAPI()



class LiftSubscriber(Node):

    def __init__(self):

        super().__init__('lift_state_subscriber')

        self.floor_name = None

        self.subscription = self.create_subscription(
        LiftState,
        '/lift_states',
        self.listener_callback,
        10)

        @app.get("/open-rmf/current_floor")
        async def get_current_floor():
            self.get_logger().info(f"============ get_current_floor: {self.floor_name}")
            # 获取当前楼层信息
            current_floor = self.floor_name
            response = {'success': False, 'msg': ''}
            if current_floor is None:
                return response
            else:
                response['success'] = True
                response['current_floor'] = current_floor
                return response



    def listener_callback(self, msg):
        self.floor_name = msg.current_floor




def main(argv=sys.argv):
# Init rclpy and adapter
    rclpy.init(args=argv)

    lift_state_subscriber = LiftSubscriber()

    spin_thread = threading.Thread(target=rclpy.spin, args=(lift_state_subscriber,))
    spin_thread.start()

    uvicorn.run(
    app,
    host='127.0.0.1',
    port=8001,
    log_level='warning',
    )


if __name__ == '__main__':
    main(sys.argv)