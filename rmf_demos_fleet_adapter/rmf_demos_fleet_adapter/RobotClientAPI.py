# Copyright 2021 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


'''
    The RobotAPI class is a wrapper for API calls to the robot. Here users
    are expected to fill up the implementations of functions which will be used
    by the RobotCommandHandle. For example, if your robot has a REST API, you
    will need to make http request calls to the appropriate endpoints within
    these functions.
'''
import requests
import nudged
import time

class RobotAPI:
    # The constructor below accepts parameters typically required to submit
    # http requests. Users should modify the constructor as per the
    # requirements of their robot's API
    def __init__(self, config_yaml):
        self.prefix = config_yaml['prefix']
        self.user = config_yaml['user']
        self.password = config_yaml['password']
        self.timeout = 5.0
        self.debug = False
        self.reference_coordinates_yaml = config_yaml['reference_coordinates']

    def check_connection(self):
        ''' Return True if connection to the robot API server is successful '''
        # ------------------------ #
        # IMPLEMENT YOUR CODE HERE #
        # ------------------------ #
        return True

    def navigate(
        self,
        robot_name: str,
        pose,
        map_name: str,
        speed_limit=0.0
    ):
        ''' Request the robot to navigate to pose:[x,y,theta] where x, y and
            and theta are in the robot's coordinate convention. This function
            should return True if the robot has accepted the request,
            else False '''
        # ------------------------ #
        # IMPLEMENT YOUR CODE HERE #
        # ------------------------ #
        response = self.send_command(pose)
        return response
    
    def send_command(self, pose):
        api_url = "http://api.quikbotdelivery.com/api/task/v1/command"
        headers = {
            "Authorization": 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhIiwiaWF0IjoxNTExNDA4MTExLCJleHAiOjE1NDI5NDQxMTEsIm5iZiI6MTUxMTQwODExMSwiYXBpS2V5IjoiMTlkYThlM2QtY2ViNS00NGU2LWFjOTItZGU0OWNiOGNiZGUxIn0.CxDddlRzwlArWsyJ2ZmNkP4e_fPiCBN2RQjIYri0AAw',
            "Content-Type": "application/json"
        }
        
        robot_tf_map = self.robot_tf()
        
        # Ensure pose parameter is a list containing position and orientation
        robot_x, robot_y = robot_tf_map.transform([pose[0], pose[1]])
        
        payload = {
            "hubId": "c023a6c4-6747-4118-b42a-78e9bcc5036c",
            "coordinates": [
                robot_x,
                robot_y,
                0,
                0,
                0,
                0,
                0,
                0
            ],
            "commandType": "MOVE",
            "entityId": "c595d399-f240-460f-8bd1-7119c1dad025"
        }
        try:
            response = requests.post(api_url, json=payload, headers=headers)
            if response.status_code == 200:
                print(f"Command sent successfully.")
            else:
                print(f"Failed to send command. Status code: {response.status_code}")
        except Exception as e:
            print(f"An error occurred: {e}")
        
        command_id = response.json()['id']
        response = {'success': False, 'msg': ''}
        response['success'] = True
        response['cmd_id'] = command_id
        return response

        

    def start_activity(
        self,
        robot_name: str,
        activity: str,
        label: str
    ):
        ''' Request the robot to begin a process. This is specific to the robot
        and the use case. For example, load/unload a cart for Deliverybot
        or begin cleaning a zone for a cleaning robot.
        Return True if process has started/is queued successfully, else
        return False '''
        # ------------------------ #
        # IMPLEMENT YOUR CODE HERE #
        # ------------------------ #
        return False

    def stop(self, robot_name: str):
        ''' Command the robot to stop.
            Return True if robot has successfully stopped. Else False. '''
        # ------------------------ #
        # IMPLEMENT YOUR CODE HERE #
        # ------------------------ #
        self.stop()
        return True
    

    def stop_command(self):
            api_url = "http://api.quikbotdelivery.com/api/task/v1/command"
            headers = {
                "Authorization": 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhIiwiaWF0IjoxNTExNDA4MTExLCJleHAiOjE1NDI5NDQxMTEsIm5iZiI6MTUxMTQwODExMSwiYXBpS2V5IjoiMTlkYThlM2QtY2ViNS00NGU2LWFjOTItZGU0OWNiOGNiZGUxIn0.CxDddlRzwlArWsyJ2ZmNkP4e_fPiCBN2RQjIYri0AAw',
                "Content-Type": "application/json"
            }
            
            payload = {
                "hubId": "c023a6c4-6747-4118-b42a-78e9bcc5036c",
                "commandType": "ABORT",
                "entityId": "c595d399-f240-460f-8bd1-7119c1dad025"
            }
            try:
                response = requests.post(api_url, json=payload, headers=headers)
                if response.status_code == 200:
                    print(f"Command sent successfully.")
                else:
                    print(f"Failed to send command. Status code: {response.status_code}")
            except Exception as e:
                print(f"An error occurred: {e}")
            
            response = {'success': False, 'msg': ''}
            response['success'] = True
            return response


    def position(self):
        ''' Return [x, y, theta] expressed in the robot's coordinate frame or
        None if any errors are encountered '''
        x, y, yaw = self.get_location()
        robot_rmf_tf = self.rmf_tf()
        rmf_x, rmf_y = robot_rmf_tf.transform([x, y])
        rmf_yaw = yaw + robot_rmf_tf.get_rotation()
        position = [rmf_x, rmf_y, rmf_yaw]
        return position
    
    
    def robot_tf(self):
        coords = self.reference_coordinates_yaml['L1']
        rmf_coords = coords['rmf']
        robot_coords = coords['robot']
        tf = nudged.estimate(rmf_coords, robot_coords)
        return tf
            
    def rmf_tf(self):
        coords = self.reference_coordinates_yaml['L1']
        rmf_coords = coords['rmf']
        robot_coords = coords['robot']
        tf = nudged.estimate(robot_coords, rmf_coords)
        return tf
    
    def get_location(self):
            try:
                api_url = "http://api.quikbotdelivery.com/api/status/v1/status/c595d399-f240-460f-8bd1-7119c1dad025"
                headers = {
                    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhIiwiaWF0IjoxNTExNDA4MTExLCJleHAiOjE1NDI5NDQxMTEsIm5iZiI6MTUxMTQwODExMSwiYXBpS2V5IjoiMTlkYThlM2QtY2ViNS00NGU2LWFjOTItZGU0OWNiOGNiZGUxIn0.CxDddlRzwlArWsyJ2ZmNkP4e_fPiCBN2RQjIYri0AAw'
                }
                response = requests.get(api_url, headers=headers)
                response.raise_for_status()  # 如果请求不成功，将引发异常
                data = response.json()
                # 获取第一个元素的 indoorLocation
                indoor_location = data['indoorLocation']
                yaw = data['coordinates'][3]
                if len(indoor_location) >= 2:
                    x, y = indoor_location[:2]  # 获取前两个元素作为 x 和 y
                    return x, y, yaw
                else:
                    return None, None, None
            except Exception as e:
                print(f"Error occurred: {e}")
                return None, None, None
            
    def battery_soc(self, robot_name: str):
        ''' Return the state of charge of the robot as a value between 0.0
        and 1.0. Else return None if any errors are encountered. '''
        # ------------------------ #
        # IMPLEMENT YOUR CODE HERE #
        # ------------------------ #
        return 1.0

    def map(self, robot_name: str):
        ''' Return the name of the map that the robot is currently on or
        None if any errors are encountered. '''
        # ------------------------ #
        # IMPLEMENT YOUR CODE HERE #
        # ------------------------ #
        floor = self.get_floor_name()
        return floor
    
    def get_floor_name(self):
   
            api_url = "http://127.0.0.1:8001/open-rmf/current_floor"
            try:
                response = requests.get(api_url,)
                if response.status_code == 200:
                    print(f"Command get successfully.")
                else:
                    print(f"Failed to get command. Status code: {response.status_code}")
            except Exception as e:
                print(f"An error occurred: {e}")
            result = {'success': False, 'msg': ''}
            data = response.json()
            return data['current_floor']

    def is_command_completed(self, cmd_id: str):
        ''' Return True if the robot has completed its last command, else
        return False. '''
        # ------------------------ #
        # IMPLEMENT YOUR CODE HERE #
        # ------------------------ #
        if(cmd_id is None):
             return False
        command = self.get_command(cmd_id)
        return command['completed']

    def get_command(self, cmd_id):
            if(cmd_id is None):
                result = {'success': False, 'msg': '', 'completed': False}
                result['completed'] = False
                return result
            api_url = "http://api.quikbotdelivery.com/api/task/v1/command/" + cmd_id
            headers = {
                "Authorization": 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhIiwiaWF0IjoxNTExNDA4MTExLCJleHAiOjE1NDI5NDQxMTEsIm5iZiI6MTUxMTQwODExMSwiYXBpS2V5IjoiMTlkYThlM2QtY2ViNS00NGU2LWFjOTItZGU0OWNiOGNiZGUxIn0.CxDddlRzwlArWsyJ2ZmNkP4e_fPiCBN2RQjIYri0AAw',
                "Content-Type": "application/json"
            }
            try:
                response = requests.get(api_url, headers=headers)
                if response.status_code == 200:
                    print(f"Command get successfully.")
                else:
                    print(f"Failed to get command. Status code: {response.status_code}")
            except Exception as e:
                print(f"An error occurred: {e}")
            result = {'success': False, 'msg': ''}
            result['success'] = True
            command_state = response.json()['commandState']
            result['completed'] = command_state == 'COMPLETED'
            return result
    
    def get_data(self, robot_name: str, node):
        ''' Returns a RobotUpdateData for one robot if a name is given. Otherwise
        return a list of RobotUpdateData for all robots. '''
        map = self.map(robot_name)
        position = self.position()
        battery_soc = self.battery_soc(robot_name)

        if not (map is None or position is None or battery_soc is None):
            return RobotUpdateData(robot_name, map, position, battery_soc)
        return None


class RobotUpdateData:
    ''' Update data for a single robot. '''
    def __init__(self,
                 robot_name: str,
                 map: str,
                 position: list[float],
                 battery_soc: float,
                 requires_replan: bool | None = None):
        self.robot_name = robot_name
        self.position = position
        self.map = map
        self.battery_soc = battery_soc
        self.requires_replan = requires_replan
