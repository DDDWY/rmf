#!/usr/bin/env python3

# Copyright 2022 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import annotations

import sys
import enum
import threading
import time

from yaml import YAMLObject
from typing import Optional
from rclpy.impl.rcutils_logger import RcutilsLogger

'''
    The LiftAPI class is a wrapper for API calls to the lift. Here users are
    expected to fill up the implementations of functions which will be used by
    the LiftAdapter. For example, if your lift has a REST API, you will need to
    make http request calls to the appropriate endpints within these functions.
'''
class DoorState(enum.IntEnum):
    CLOSED = 0
    MOVING = 1
    OPEN = 2


class MotionState(enum.IntEnum):
    STOPPED = 0
    UP = 1
    DOWN = 2
    UNKNOWN = 3


class LiftStatus:
    def __init__(self, current_floor='L1', motion_state=MotionState.STOPPED, door_state=DoorState.CLOSED):
        self.current_floor = current_floor
        self.motion_state = motion_state
        self.door_state = door_state

    def __str__(self):
        # if self.motion_state == MotionState.STOPPED:
        #     motion = 'STOPPED'
        # elif self.motion_state == MotionState.UP:
        #     motion = 'UP'
        # elif self.motion_state == MotionState.DOWN:
        #     motion = 'DOWN'
        # else:
        #     motion = 'UNKNOWN'
        # if self.door_state == DoorState.CLOSED:
        #     door = 'CLOSED'
        # elif self.door_state == DoorState.OPEN:
        #     door = 'OPEN'
        # else:
        #     door = 'MOVING'
        return f'# lift status: current_floor: {self.current_floor}, door: {self.door_state.name}, motion: {self.motion_state.name}'

    def __repr__(self):
        return self.__str__()


class LiftAPI:
    # The constructor accepts a safe loaded YAMLObject, which should contain all
    # information that is required to run any of these API calls.
    def __init__(self, name: str, config: dict, logger: RcutilsLogger):
        print(f'## {config}')
        self.name = name
        self.device_id = config['lift']['id']
        self.floors = config['lift']['available_floors']
        broker = config['mqtt']['broker']
        port = config['mqtt']['port']
        username = config['mqtt'].get('username')
        password = config['mqtt'].get('password')
        print(f'######### {self.floors=}')
        self.logger = logger
        self.status = LiftStatus(self.floors[0], MotionState.STOPPED, DoorState.CLOSED)

        self._destination_floor = self.floors[0]
        self._destination_door_state = DoorState.CLOSED

        threading.Thread(target=self.lift_run, daemon=True).start()

        # Test initial connectivity
        self.logger.info('Checking connectivity.')
        if not self.check_connection():
            self.logger.error('Failed to establish connection with lift')
            sys.exit(1)

    def log_status(self):
        pass
        # self.logger.info(f'lift-{self.name} status: {self.status}')

    def lift_run(self):
        while True:
            if self.status.current_floor == self._destination_floor:
                self.status.motion_state = MotionState.STOPPED
                if self.status.door_state != self._destination_door_state and self.status.door_state != DoorState.MOVING:
                    self.status.door_state = DoorState.MOVING
                    self.log_status()
                    time.sleep(5)
                elif self.status.door_state != self._destination_door_state and self.status.door_state == DoorState.MOVING:
                    self.status.door_state = self._destination_door_state
                    time.sleep(1)
                    self.log_status()
                    # if self.status.door_state == DoorState.OPEN:
                    #     self._destination_door_state = DoorState.CLOSED
                    #     self.log_status()
                    #     time.sleep(60)
                else:
                    self.log_status()
                    time.sleep(1)
            else:
                if self.status.door_state == DoorState.OPEN:
                    self.status.door_state = DoorState.MOVING
                    self.log_status()
                    time.sleep(2)
                    self.status.door_state = DoorState.CLOSED
                floor1 = int(self.status.current_floor[1:])
                floor2 = int(self._destination_floor[1:])
                if floor1 < floor2:
                    self.status.motion_state = MotionState.UP
                else:
                    self.status.motion_state = MotionState.DOWN
                self.log_status()
                time.sleep(3)
                self.status.current_floor = self._destination_floor
                time.sleep(2)
                self.log_status()

    def check_connection(self) -> bool:
        ''' Return True if connection to the lift is successful'''
        # return self.lift_client.is_connected()
        return True

    def available_floors(self) -> Optional[list[str]]:
        ''' Returns the available floors for this lift, or None the query
            failed'''
        return self.floors

    def current_floor(self) -> Optional[str]:
        ''' Returns the current floor of this lift, or None the query failed'''
        return self.status.current_floor

    def destination_floor(self) -> Optional[str]:
        ''' Returns the destination floor of this lift, or None the query
            failed'''
        return self._destination_floor

    def lift_door_state(self) -> Optional[int]:
        ''' Returns the state of the lift door, based on the static enum
            LiftState.DOOR_*, or None the query failed'''
        return self.status.door_state

    def lift_motion_state(self) -> Optional[int]:
        ''' Returns the lift cabin motion state, based on the static enum
            LiftState.MOTION_*, or None the query failed'''
        return self.status.motion_state

    def command_lift(self, floor: str, door_state) -> bool:
        ''' Sends the lift cabin to a specific floor and opens all available
            doors for that floor. Returns True if the request was sent out
            successfully, False otherwise

            uint8 door_state
            uint8 DOOR_CLOSED=0
            uint8 DOOR_MOVING=1
            uint8 DOOR_OPEN=2

            uint8 motion_state
            uint8 MOTION_STOPPED=0
            uint8 MOTION_UP=1
            uint8 MOTION_DOWN=2
            uint8 MOTION_UNKNOWN=3
        '''
        try:
            self._destination_floor = floor
            if door_state == 0:
                self._destination_door_state = DoorState.CLOSED
            else:
                self._destination_door_state = DoorState.OPEN
            self.logger.info(f"Sending lift to {floor}, door state: {self._destination_door_state.name}")
            return True
        except Exception as e:
            self.logger.error(e.args[0])
            return False
