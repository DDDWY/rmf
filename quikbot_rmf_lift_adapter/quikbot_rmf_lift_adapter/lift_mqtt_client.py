
import uuid
import json
import threading
import enum
from copy import deepcopy

import paho.mqtt.client as mqtt
from rclpy.impl.rcutils_logger import RcutilsLogger


class DoorState(enum.IntEnum):
    CLOSED = 0
    MOVING = 1
    OPEN = 2


class MotionState(enum.IntEnum):
    STOPPED = 0
    UP = 1
    DOWN = 2
    UNKNOWN = 3


class LiftStatus:
    def __init__(self, floor_name="L1", door_state=DoorState.CLOSED, motion_state=MotionState.STOPPED):
        self.floor_name: str = floor_name
        self.door_state: DoorState = door_state
        self.motion_state: MotionState = motion_state


class LiftMqttClient:
    def __init__(self, device_id, broker, port, username, password, logger: RcutilsLogger):
        self.logger = logger
        client_id = device_id
        self.device_id = device_id
        self.client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2, client_id)
        if username and password:
            self.client.username_pw_set(username=username, password=password)
        self.client.connect(broker, port)
        self.status = LiftStatus()
        self.lock = threading.Lock()
        self.status_topic = f'/status-update-production/{device_id}'
        self.cmd_topic = f'/command-production/{device_id}'
        self.start_status_subscription()
        # self.client.loop_forever()
        threading.Thread(target=lambda: self.client.loop_forever(), daemon=True).start()

    def get_lift_status(self):
        with self.lock:
            return deepcopy(self.status)

    def is_connected(self):
        return self.client.is_connected()

    def call_lift(self, floor_name):
        msg = {
            "id": str(uuid.uuid4()),
            "entityId": self.device_id,
            "commandType": "ELEVATOR_FLOOR",
            "floorName": floor_name
        }
        self.publish(self.cmd_topic, msg)

    def control_door(self, operation_type, seconds=5):
        msg = {
            "id": str(uuid.uuid4()),
            "entityId": self.device_id,
            "commandType": "ELEVATOR_DOOR",
            "operationType": operation_type,
            "controlTime": seconds
        }
        self.publish(self.cmd_topic, msg)

    def publish(self, topic, msg):
        self.logger.info(f'publish message {msg}')
        result = self.client.publish(topic, json.dumps(msg))
        status = result[0]
        if status != 0:
            msg = f"Failed to send message to topic {topic}, msg: {msg}"
            self.logger.error(msg)
            raise RuntimeError(msg)

    def start_status_subscription(self):
        def on_message(client, userdata, msg):
            try:
                status = LiftStatus()
                data = json.loads(msg.payload.decode())
                # self.logger.info(f"{data=}")
                elevator = data.get("elevator")
                door = data.get("door")
                if elevator:
                    # status.floor_name = elevator["floorName"]
                    # motion = elevator["elevatorDirection"]
                    # if motion == "STOP":
                    #     status.motion_state = MotionState.STOPPED
                    # elif motion == "UP":
                    #     status.motion_state = MotionState.UP
                    # elif motion == "DOWN":
                    #     status.motion_state = MotionState.DOWN
                    # else:
                    #     status.motion_state = MotionState.UNKNOWN
                    if elevator['raw'] == '0':
                        status.floor_name = "L1"
                    else:
                        status.floor_name = "L2"
                else:
                    self.logger.warning(f"missing elevator state in topic {self.status_topic}")
                if door:
                    # s = door["doorState"]
                    # if s == "OPEN":
                    #     status.door_state = DoorState.OPEN
                    # elif s == "CLOSED":
                    #     status.door_state = DoorState.CLOSED
                    # elif s == "OPENING" or s == "CLOSING":
                    #     status.door_state = DoorState.MOVING
                    # else:
                    #     status.door_state = DoorState.CLOSED
                    if door['raw'] == '0':
                        status.door_state = DoorState.CLOSED
                    else:
                        status.door_state = DoorState.OPEN
                with self.lock:
                    self.status = status
            except Exception as e:
                self.logger.warning(f"on message error, {e.args[0]}")
        self.client.on_message = on_message
        self.client.subscribe(self.status_topic, qos=1)


if __name__ == '__main__':
    from loguru import logger
    client = LiftMqttClient(device_id="278ce055-3da1-4618-ba81-162695651cce",
                            broker="mqtt.quikbot.ai",
                            port=1883,
                            username="admin",
                            password="sETjJT6LcjVqhmyp",
                            logger=logger)