#!/usr/bin/env python3

# Copyright 2022 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import annotations

import sys
from yaml import YAMLObject
from typing import Optional
from rclpy.impl.rcutils_logger import RcutilsLogger
from quikbot_rmf_lift_adapter.lift_mqtt_client import LiftMqttClient

'''
    The LiftAPI class is a wrapper for API calls to the lift. Here users are
    expected to fill up the implementations of functions which will be used by
    the LiftAdapter. For example, if your lift has a REST API, you will need to
    make http request calls to the appropriate endpints within these functions.
'''


class LiftAPI:
    # The constructor accepts a safe loaded YAMLObject, which should contain all
    # information that is required to run any of these API calls.
    def __init__(self, name: str, config: dict, logger: RcutilsLogger):
        print(f'## {config}')
        self.name = name
        self.device_id = config['lift']['id']
        self.floors = config['lift']['available_floors']
        broker = config['mqtt']['broker']
        port = config['mqtt']['port']
        username = config['mqtt'].get('username')
        password = config['mqtt'].get('password')
        print(f'######### {self.floors=}')
        self.logger = logger
        self.lift_client = LiftMqttClient(device_id=self.device_id,
                                          broker=broker,
                                          port=port,
                                          username=username,
                                          password=password,
                                          logger=logger)

        self._destination_floor = self.lift_client.get_lift_status().floor_name
        # Test initial connectivity
        self.logger.info('Checking connectivity.')
        if not self.check_connection():
            self.logger.error('Failed to establish connection with lift')
            sys.exit(1)

    def check_connection(self) -> bool:
        ''' Return True if connection to the lift is successful'''
        # return self.lift_client.is_connected()
        return True

    def available_floors(self) -> Optional[list[str]]:
        ''' Returns the available floors for this lift, or None the query
            failed'''
        return self.floors

    def current_floor(self) -> Optional[str]:
        ''' Returns the current floor of this lift, or None the query failed'''
        return self.lift_client.get_lift_status().floor_name

    def destination_floor(self) -> Optional[str]:
        ''' Returns the destination floor of this lift, or None the query
            failed'''
        return self._destination_floor

    def lift_door_state(self) -> Optional[int]:
        ''' Returns the state of the lift door, based on the static enum
            LiftState.DOOR_*, or None the query failed'''
        return self.lift_client.get_lift_status().door_state

    def lift_motion_state(self) -> Optional[int]:
        ''' Returns the lift cabin motion state, based on the static enum
            LiftState.MOTION_*, or None the query failed'''
        return self.lift_client.get_lift_status().motion_state

    def command_lift(self, floor: str) -> bool:
        ''' Sends the lift cabin to a specific floor and opens all available
            doors for that floor. Returns True if the request was sent out
            successfully, False otherwise'''
        try:
            self.logger.info(f"Sending lift to {floor}")
            self._destination_floor = floor
            self.lift_client.call_lift(floor)
            return True
        except Exception as e:
            self.logger.error(e.args[0])
            return False
